node-nodemailer (6.10.0+~6.4.17-1) unstable; urgency=medium

  * New upstream version 6.10.0+~6.4.17

 -- Yadd <yadd@debian.org>  Sun, 26 Jan 2025 19:09:53 +0100

node-nodemailer (6.9.16+~6.4.16-1) unstable; urgency=medium

  * New upstream version 6.9.16+~6.4.16

 -- Yadd <yadd@debian.org>  Thu, 31 Oct 2024 06:56:03 +0100

node-nodemailer (6.9.15+~6.4.16-1) unstable; urgency=medium

  * New upstream version 6.9.15+~6.4.16

 -- Yadd <yadd@debian.org>  Sat, 21 Sep 2024 07:01:00 +0400

node-nodemailer (6.9.13+~6.4.14-1) unstable; urgency=medium

  * Declare compliance with policy 4.7.0
  * New upstream version 6.9.13+~6.4.14
  * Update docs
  * Update test modules
  * Refresh patches
  * Test dependencies: drop mocha, add punycode

 -- Yadd <yadd@debian.org>  Mon, 22 Apr 2024 14:53:13 +0400

node-nodemailer (6.9.4+~6.4.9-4) unstable; urgency=medium

  * Drop proxy test (Closes: #1056709)

 -- Yadd <yadd@debian.org>  Thu, 07 Dec 2023 12:05:59 +0400

node-nodemailer (6.9.4+~6.4.9-3) unstable; urgency=medium

  * Add patch for node-proxy >= 2

 -- Yadd <yadd@debian.org>  Thu, 23 Nov 2023 11:54:51 +0400

node-nodemailer (6.9.4+~6.4.9-2) unstable; urgency=medium

  * Drop one test based on timeout

 -- Yadd <yadd@debian.org>  Mon, 21 Aug 2023 17:40:54 +0400

node-nodemailer (6.9.4+~6.4.9-1) unstable; urgency=medium

  * Declare compliance with policy 4.6.2
  * New upstream version 6.9.4+~6.4.9 (Closes: #1034851)
  * Allow stderr in network test
  * Drop patch

 -- Yadd <yadd@debian.org>  Sat, 12 Aug 2023 16:54:05 +0400

node-nodemailer (6.8.0+~6.4.6-1) unstable; urgency=medium

  * New upstream version 6.8.0+~6.4.6

 -- Yadd <yadd@debian.org>  Fri, 30 Sep 2022 12:37:20 +0200

node-nodemailer (6.7.8+~6.4.5-2) unstable; urgency=medium

  * Drop another timeout based test

 -- Yadd <yadd@debian.org>  Tue, 16 Aug 2022 10:30:36 +0200

node-nodemailer (6.7.8+~6.4.5-1) unstable; urgency=medium

  * New upstream version 6.7.8+~6.4.5

 -- Yadd <yadd@debian.org>  Mon, 15 Aug 2022 07:01:33 +0200

node-nodemailer (6.7.7+~6.4.4-1) unstable; urgency=medium

  * New upstream version 6.7.7+~6.4.4

 -- Yadd <yadd@debian.org>  Sat, 09 Jul 2022 08:57:12 +0200

node-nodemailer (6.7.6+~6.4.4-1) unstable; urgency=medium

  * Declare compliance with policy 4.6.1
  * New upstream version 6.7.6+~6.4.4
  * Update test modules (Closes: #1013994)
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Mon, 04 Jul 2022 07:31:58 +0200

node-nodemailer (6.7.5+~6.4.4-1) unstable; urgency=medium

  * Replace some embedded test modules by dependencies to new packages
  * New upstream version 6.7.5+~6.4.4

 -- Yadd <yadd@debian.org>  Thu, 05 May 2022 10:46:02 +0200

node-nodemailer (6.7.4+~6.4.4-1) unstable; urgency=medium

  * New upstream version 6.7.4+~6.4.4
  * Unfuzz patches

 -- Yadd <yadd@debian.org>  Sun, 01 May 2022 09:38:18 +0200

node-nodemailer (6.7.3+~6.4.4-1) unstable; urgency=medium

  * New upstream version 6.7.3+~6.4.4

 -- Yadd <yadd@debian.org>  Tue, 22 Mar 2022 06:30:21 +0100

node-nodemailer (6.7.2+~6.4.4-1) unstable; urgency=medium

  * New upstream version 6.7.2+~6.4.4

 -- Yadd <yadd@debian.org>  Tue, 30 Nov 2021 09:32:23 +0100

node-nodemailer (6.7.1+~6.4.4-1) unstable; urgency=medium

  * Embed typescript definitions
  * New upstream version 6.7.1+~6.4.4

 -- Yadd <yadd@debian.org>  Wed, 17 Nov 2021 06:20:08 +0100

node-nodemailer (6.7.0-2) unstable; urgency=medium

  * Fix debian/watch
  * Update lintian overrides
  * Clean test modules

 -- Yadd <yadd@debian.org>  Fri, 05 Nov 2021 12:24:47 +0100

node-nodemailer (6.7.0-1) unstable; urgency=medium

  * New upstream version 6.7.0
  * Split autopkgtest: deparate network tests
  * Drop nodejs dependency

 -- Yadd <yadd@debian.org>  Tue, 12 Oct 2021 18:30:06 +0200

node-nodemailer (6.6.5-1) unstable; urgency=medium

  * Declare compliance with policy 4.6.0
  * New upstream version 6.6.5
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Sun, 26 Sep 2021 07:39:06 +0200

node-nodemailer (6.6.3-1) unstable; urgency=medium

  * New upstream version 6.6.3
  * Back to unstable

 -- Yadd <yadd@debian.org>  Sat, 14 Aug 2021 13:26:31 +0200

node-nodemailer (6.6.1-1) experimental; urgency=medium

  * New upstream version 6.6.1
  * Remove patch

 -- Yadd <yadd@debian.org>  Wed, 30 Jun 2021 15:09:12 +0200

node-nodemailer (6.4.17-3) unstable; urgency=medium

  * Fix GitHub tags regex
  * Fix header injection vulnerability in address object
    (Closes: #990485, CVE-2021-23400)

 -- Yadd <yadd@debian.org>  Wed, 30 Jun 2021 14:59:47 +0200

node-nodemailer (6.4.17-2) unstable; urgency=medium

  * Ignore cookie test (Closes: #980702)

 -- Xavier Guimard <yadd@debian.org>  Thu, 21 Jan 2021 06:26:01 +0100

node-nodemailer (6.4.17-1) unstable; urgency=medium

  * New upstream version 6.4.17

 -- Xavier Guimard <yadd@debian.org>  Tue, 15 Dec 2020 05:46:32 +0100

node-nodemailer (6.4.16-1) unstable; urgency=medium

  [ lintian-brush ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Xavier Guimard ]
  * New upstream version 6.4.16 (Closes: CVE-2020-7769)

 -- Xavier Guimard <yadd@debian.org>  Thu, 12 Nov 2020 14:46:22 +0100

node-nodemailer (6.4.14-1) unstable; urgency=medium

  * New upstream version 6.4.14

 -- Xavier Guimard <yadd@debian.org>  Sun, 18 Oct 2020 14:10:50 +0200

node-nodemailer (6.4.13-1) unstable; urgency=medium

  * Bump debhelper compatibility level to 13
  * Use dh-sequence-nodejs
  * New upstream version 6.4.13
  * Clean test modules
  * Update lintian overrides

 -- Xavier Guimard <yadd@debian.org>  Thu, 08 Oct 2020 13:20:42 +0200

node-nodemailer (6.4.6-1) unstable; urgency=medium

  * New upstream version 6.4.6
  * Rename debian/tests/node_modules to debian/tests/test_modules to use
    pkg-js-tools 0.9.35 auto links
  * Clean test embedded modules
  * Add build dependency to node-commander, node-debug, node-iconv-lite,
    node-ms
  * Update copyright

 -- Xavier Guimard <yadd@debian.org>  Wed, 20 May 2020 23:48:45 +0200

node-nodemailer (6.4.5-1) unstable; urgency=medium

  * New upstream version 6.4.5

 -- Xavier Guimard <yadd@debian.org>  Fri, 13 Mar 2020 15:36:13 +0100

node-nodemailer (6.4.4-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 6.4.4
  * Bump standards version to 4.5.0
  * Fix Lintian

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 08 Mar 2020 23:44:50 +0530

node-nodemailer (6.4.2-1) unstable; urgency=medium

  * New upstream version 6.4.2
  * Increase timeout (may fix debci)

 -- Xavier Guimard <yadd@debian.org>  Sun, 15 Dec 2019 09:20:35 +0100

node-nodemailer (6.4.1-1) unstable; urgency=medium

  * New upstream version 6.4.1

 -- Xavier Guimard <yadd@debian.org>  Wed, 11 Dec 2019 05:33:16 +0100

node-nodemailer (6.4.0-1) unstable; urgency=medium

  * New upstream version 6.4.0
  * Update lintian overrides
  * Switch to debhelper-compat

 -- Xavier Guimard <yadd@debian.org>  Sat, 07 Dec 2019 08:36:46 +0100

node-nodemailer (6.3.1-1) unstable; urgency=medium

  * Declare compliance with policy 4.4.1
  * New upstream version 6.3.1
  * Drop changelog patch

 -- Xavier Guimard <yadd@debian.org>  Sat, 12 Oct 2019 10:07:05 +0200

node-nodemailer (6.3.0-2) unstable; urgency=medium

  * Switch install to pkg-js-tools

 -- Xavier Guimard <yadd@debian.org>  Sat, 03 Aug 2019 15:43:31 +0200

node-nodemailer (6.3.0-1) unstable; urgency=medium

  * New upstream version 6.3.0
  * Add missing upstream changelog entry
  * Add Multi-Arch: foreign

 -- Xavier Guimard <yadd@debian.org>  Tue, 23 Jul 2019 07:25:11 +0200

node-nodemailer (6.2.1-1) unstable; urgency=medium

  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.0
  * Move installed files to /usr/share/nodejs
  * New upstream version 6.2.1

 -- Xavier Guimard <yadd@debian.org>  Wed, 10 Jul 2019 06:22:05 +0200

node-nodemailer (5.1.1-1) unstable; urgency=low

  * Initial release (Closes: #924486)

 -- Xavier Guimard <yadd@debian.org>  Wed, 13 Mar 2019 14:28:01 +0100
